program bcc

implicit none

integer(kind=8) :: nside,nsite,nelec
integer(kind=8) :: ix,iy,iz,i
real*16  :: factor,bbfac
real*16  :: uee,ubb,eta0,aux
real*16  :: start,finish
real*16,allocatable  :: sin2(:)

open(unit=10,file='input')
   read(10,*) nside
close(10)

call cpu_time(start)

nsite = 2*nside
nelec = nside**3*2

bbfac = 1.4305055275019529822q0

write(6,*) "total number of electrons",nelec

allocate(sin2(nsite))

factor=acos(-1q0)/nsite
do i = 0, nsite-1
   sin2(i+1) = (sin(factor*i))**2
enddo

uee = 0q0
do ix = 0, nsite-1
   do iy = 0, nsite-1
      do iz = 0, nsite-1
         if (ix+iy+iz==0) cycle
         if (mod(ix+iy,2)==1.or.mod(ix+iz,2)==1.or.mod(iy+iz,2)==1) cycle
         aux = sqrt(sin2(ix+1)+sin2(iy+1)+sin2(iz+1))
         uee = uee + 1q0/aux
      enddo
   enddo
enddo

uee=uee*acos(-1q0)/(2q0*nsite)

ubb = nelec*bbfac/nsite

eta0 = uee - ubb

factor=(acos(-1q0)/3q0)**(1q0/3q0)
eta0=eta0/factor

print*,'eta0 (in Ry and Ha) =', 2q0*eta0, eta0

deallocate(sin2)

call cpu_time(finish)
print '("Time = ",f12.3," seconds.")',finish-start

end program bcc
