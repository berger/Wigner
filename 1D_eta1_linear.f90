program linear

implicit none

integer :: nelec
integer :: i,k
real*8  :: factor
real*8  :: zpe
real*8,allocatable :: cmat(:)
real*8,allocatable :: omega2(:)

write(6,*) "number of electrons"
read(5,*) nelec

allocate (cmat(nelec))
allocate (omega2(nelec))

factor=acos(-1d0)/nelec
cmat = 0d0
do i = 1, nelec
   cmat(1) = cmat(1) + (1d0 + cos(factor*i))**2/sin(factor*i)**3
enddo
do i = 1, nelec-1
   cmat(i+1) = -(1d0 + cos(factor*i))**2/sin(factor*i)**3
enddo
cmat = cmat*factor**3/8d0

factor = 2*acos(-1d0)/nelec
omega2 = 0d0
do k = 0, nelec-1
   do i = 0, nelec-1
      omega2(k+1) = omega2(k+1) + cmat(i+1)*cos(factor*k*i)
   enddo
enddo

zpe=0d0
do k = 1, nelec
   if (omega2(k)<0d0) cycle
   zpe = zpe + sqrt(omega2(k))
enddo
zpe=zpe/nelec

print*,'zero-point energy (in Ry and Ha) =',zpe,zpe/2

deallocate(cmat,omega2)

end program linear
